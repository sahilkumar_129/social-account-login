import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import * as GoogleTokenStrategy from 'passport-google-id-token';

@Injectable()
export class GoogleStrategy extends PassportStrategy(
  GoogleTokenStrategy,
  'google-id-token',
) {
  constructor() {
    super({
      clientID: process.env.GOOGLE_CLIENT_ID,
      // clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      // scope: ['email', 'profile'],
    });
  }

  async validate(
    parsedToken: any,
    googleId: string,
    done: (err: any, user: any, info?: any) => void,
  ): Promise<any> {
    const {
      given_name: first_name,
      family_name: last_name,
      email,
    } = parsedToken.payload;
    const customer = {
      first_name,
      last_name,
      email,
    };
    return done(null, customer);
  }
}
