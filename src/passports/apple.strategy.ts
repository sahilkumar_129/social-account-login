import { Injectable, Logger } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import * as AppleAuthStrategy from 'passport-apple';

@Injectable()
export class AppleStrategy extends PassportStrategy(
  AppleAuthStrategy,
  'apple',
) {
  constructor() {
    super({
      clientID: process.env.APPLE_BUNDLE_ID,
      teamID: process.env.APPLE_TEAM_ID,
      callbackURL: '',
      keyID: process.env.APPLE_KEY_ID,
      privateKeyLocation: './AuthKey_P3JG9A9N23.p8',
      passReqToCallback: false,
    });
  }

  async validate(
    req,
    accessToken,
    refreshToken,
    decodedIdToken,
    profile,
    done: (err: any, user: any, info?: any) => void,
  ): Promise<any> {
    Logger.warn({
      provider: 'AppleAuth',
      req,
      accessToken,
      refreshToken,
      decodedIdToken,
      profile,
    });

    return done(null, {});
  }
}
