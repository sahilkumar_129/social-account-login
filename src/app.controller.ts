import {
  Controller,
  Post,
  Query,
  Req,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AccessTokenQueryDto } from './dtos/accessTokenQuery.dto';
import { AppService } from './app.service';
import { AppleTokenAuthGuard } from './guards/apple-token-auth.guard';
import { FacebookTokenAuthGuard } from './guards/facebook-token-auth.guard';
import { GoogleTokenAuthGuard } from './guards/google-token-auth.guard';

@ApiTags('Social account login')
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @UseGuards(GoogleTokenAuthGuard)
  @Post('google-login')
  public googleLogin(
    @Req() req: any,
    @Query(new ValidationPipe({ transform: true }))
    query: AccessTokenQueryDto,
  ): any {
    return this.appService.googleLogin(req.user);
  }

  @UseGuards(FacebookTokenAuthGuard)
  @Post('facebook-login')
  public facebookLogin(
    @Req() req: any,
    @Query(new ValidationPipe({ transform: true }))
    query: AccessTokenQueryDto,
  ): any {
    return this.appService.facebookLogin(req.user);
  }

  @UseGuards(AppleTokenAuthGuard)
  @Post('apple-login')
  public appleLogin(
    @Req() req: any,
    @Query(new ValidationPipe({ transform: true }))
    query: AccessTokenQueryDto,
  ): any {
    return this.appService.appleLogin(req.user);
  }
}
