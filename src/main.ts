import 'dotenv/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as express from 'express';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  if (process.env.NODE_ENV && process.env.NODE_ENV === 'DEV') {
    const swaggerOptions = new DocumentBuilder()
      .setTitle('Account Management Service')
      .setDescription('Account Management Service API description')
      .setVersion('1.0')
      .addBearerAuth()
      .build();
    const swaggerDocument = SwaggerModule.createDocument(app, swaggerOptions);
    SwaggerModule.setup('api', app, swaggerDocument);
  }
  app.use(express.json());
  await app.listen(3000);
}
bootstrap();
