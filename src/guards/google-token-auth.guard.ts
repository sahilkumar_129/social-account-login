import {
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class GoogleTokenAuthGuard extends AuthGuard('google-id-token') {
  // IMPORTANT - Keeping this commented code for future debugging
  canActivate(context: ExecutionContext) {
    // Add your custom authentication logic here
    // for example, call super.logIn(request) to establish a session.
    console.log('Inside Google Guard');
    return super.canActivate(context);
  }
  handleRequest(err, user, info) {
    console.log(err, user, info);
    // You can throw an exception based on either "info" or "err" arguments
    if (err || !user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
